# CorrectedDiffusion

This project is in support of a new PBC wrapping algorithm.
There are two implementations copied here, one based on [PBCTools](https://github.com/frobnitzem/pbctools), and one that adapts [qwrap](https://github.com/jhenin/qwrap).
These are also available on [github](https://github.com/kulkem) directly.
Installation would follow the existing documentation available from those sources.
